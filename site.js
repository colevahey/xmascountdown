// Register the service worker
if ('serviceWorker' in navigator) {
  navigator.serviceWorker.register('/service-worker.js').then(function(registration) {
    // Registration was successful
    console.log('ServiceWorker registration successful with scope: ', registration.scope);
  }).catch(function(err) {
    // registration failed :(
    console.log('ServiceWorker registration failed: ', err);
  });
}

const set_time = _ => {
    let now = new Date()
    let christmas = new Date(`${now.getFullYear()}/12/25 00:00:00`)
    let delta_time = (christmas - now) >= 0 ? christmas - now : new Date(`${now.getFullYear()+1}/12/25 00:00:00`) - now
    
    d = Math.floor(delta_time/1000/60/60/24)
    delta_time -= d*1000*60*60*24
    h = Math.floor(delta_time/1000/60/60)
    delta_time -= h*1000*60*60
    m = Math.floor(delta_time/1000/60)
    delta_time -= m*1000*60
    s = Math.floor(delta_time/1000)
    
    if (now.getMonth() == 11 && now.getDate() == 25) {
        // Christmas!
        d = 0
        h = 0
        m = 0
        s = 0
        document.querySelector("#christmasbanner").style.display = "block"
        let audio = document.querySelector("#music")
        audio.onerror = _ => console.log("LOADING MUSIC")
        document.onClick = audio.paused ? audio.play() : audio.pause()
    }

    document.querySelector("#innerDays").innerHTML = d
    document.querySelector("#innerHours").innerHTML = h
    document.querySelector("#innerMinutes").innerHTML = m
    document.querySelector("#innerSeconds").innerHTML = s
}

setInterval(set_time,100)
